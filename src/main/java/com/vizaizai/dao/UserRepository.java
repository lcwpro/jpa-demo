package com.vizaizai.dao;

import com.vizaizai.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author liaochongwei
 * @date 2022/11/5 12:40
 */
public interface UserRepository extends JpaRepository<User,String> {

}
