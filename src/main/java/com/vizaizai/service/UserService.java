package com.vizaizai.service;

import com.vizaizai.entity.User;

import java.util.List;

/**
 * @author liaochongwei
 * @date 2022/11/5 12:43
 */
public interface UserService {
    List<User> listAll();
}
