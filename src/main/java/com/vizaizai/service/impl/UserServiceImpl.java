package com.vizaizai.service.impl;

import com.vizaizai.dao.UserRepository;
import com.vizaizai.entity.User;
import com.vizaizai.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liaochongwei
 * @date 2022/11/5 12:44
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserRepository userRepository;
    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }
}
