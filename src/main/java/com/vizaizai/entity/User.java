package com.vizaizai.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author liaochongwei
 * @date 2022/11/5 12:10
 */
@Entity
@Getter
@Setter
@Table(name="user_1")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String name;
}
