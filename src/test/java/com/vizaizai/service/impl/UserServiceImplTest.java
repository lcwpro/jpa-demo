package com.vizaizai.service.impl;

import com.vizaizai.dao.UserRepository;
import com.vizaizai.entity.User;
import com.vizaizai.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Resource
    private UserService userService;
    @Test
    public void listAll() {

        List<User> users = userService.listAll();
        System.out.println(users.size());
    }
}